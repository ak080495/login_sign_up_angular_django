import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



import { User } from '../user';
import { UserService } from '../user.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  registerForm: FormGroup;
  // submitted = false;



/////////////////////////

user: User = new User();
submitted = false;



///////////////////////////

  constructor(private formBuilder: FormBuilder,private userService: UserService) { }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
  });
}



///////////////////////////////////////////////////////////////////////////



newUser(): void {
  this.submitted = false;
  this.user = new User();
}

save() {
  this.userService.createUser(this.user)
    .subscribe(
      data => {
        console.log(data);
        this.submitted = true;
      },
      error => console.log(error));
  this.user = new User();
}

onSubmit() {
  this.save();
}

















//////////////////////////////////////////////////////////////////////////////////

// convenience getter for easy access to form fields



// get f() { return this.registerForm.controls; }

// onSubmit() {
//   this.submitted = true;

//   // stop here if form is invalid
//   if (this.registerForm.invalid) {
//       return;
//   }

//   alert('SUCCESS!! :-)')
// }
  

// }




















// import { Component, OnInit } from '@angular/core';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// @Component({
//   selector: 'app-signup',
//   templateUrl: './signup.component.html',
//   styleUrls: ['./signup.component.css']
// })
// export class SignupComponent implements OnInit {
//   registerForm: FormGroup;
//   submitted = false;


//   constructor(private formBuilder: FormBuilder) { }

//   ngOnInit() {

//     this.registerForm = this.formBuilder.group({
//       firstName: ['', Validators.required],
//       lastName: ['', Validators.required],
//       email: ['', [Validators.required, Validators.email]],
//       password: ['', [Validators.required, Validators.minLength(6)]]
//   });
// }

// /convenience getter for easy access to form fields
// get f() { return this.registerForm.controls; }

// onSubmit() {
//   this.submitted = true;

//   /stop here if form is invalid
//   if (this.registerForm.invalid) {
//       return;
//   }

//   alert('SUCCESS!! :-)')
// }
  

}
