import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'http://localhost:8000/api';
  // httpheades = new HttpHeaders({'Content-type' : 'application/json'})


  constructor(private http: HttpClient) { }


createUser(user: User) {
    return this.http.post(`${this.baseUrl}/sign_up/`, user);
  }

getAll() {
  return this.http.get<User[]>(`${this.baseUrl}/users/`);
}

getById(id: number) {
  return this.http.get(`${this.baseUrl}/users/${id}`);
}
delete(id: number)  {
  return this.http.delete(`${this.baseUrl}/users/${id}`);
}
}
