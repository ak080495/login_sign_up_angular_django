import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SignupComponent } from './signup/signup.component';

import { SigninComponent } from './signin/signin.component';
import { HomeComponent } from './home/home.component';
// import { AuthGuard } from '../app/_guards/auth.guard';






const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login/signup', component: SignupComponent },
  { path: 'login', component: SigninComponent },
  { path: 'home', component: HomeComponent },
  // { path: '**', redirectTo: '../' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

